#include<iostream>
using namespace std;


int main()
{
   char ans;
   int dest,i,j,v,s,min_dist = 100000,mark_node,m;
   cout<<"Enter the no of vertices: ";
   cin>>v;
   bool done[v];
   int d[v][v],dist[v];
    cout<<"Enter values\n";
    for(i=0;i<v;i++)
        for(j=0;j<v;j++){
            cin>>d[i][j];
            if(d[i][j]<0){
                cout<<"Sorry!!! Enter only positive value\n";
                cin>>d[i][j];
            }
        }

    do{

    cout<<"Enter source: ";
    cin>>s;
    cout<<"Enter Destination: ";
    cin>>dest;

    for(i=0;i<v;i++)
        done[i]=false;
    
    for(i=0;i<v;i++)
        dist[i]=100000;
    dist[s]=0;
 int count = 0;

   while(count < v) {
       min_dist = 100000;
    	for(int m=0;m<v;m++) {
		if((!done[m]) && ( min_dist >= dist[m])) {
			min_dist = dist[m];
		 	mark_node = m;
	}

    }
  
    done[mark_node] = true;

    for(int i=0;i<v;i++) {
    	if((!done[i]) && (d[mark_node][i]>0) ) {
		if(dist[i] > dist[ mark_node]+d[ mark_node][i]) {
			dist[i] = dist[ mark_node]+d[ mark_node][i];
		}
      	}
     }
	
    count++;
    }

    i=dest;
    cout<<"The minimum distance between "<<s<<" and "<<dest<<" is "<<dist[i]<<"\n";
    cout<<"Do you want to continue? ";
    cin>>ans;
    }while(ans!='n');
  return 0;
}  
